import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

public class Ejercicio2 {
    public static void main(String[] args) throws IOException {
        String sisOp = System.getProperty("os.name").toLowerCase();
        String comando = "";

        //Se comprueba el sistema operativo para marcar la ruta
        if(sisOp.equals("windows 10")){
            comando = "java -jar ..\\psppractica2\\out\\artifacts\\Random10_jar\\psppractica2.jar";
        }else{
            comando = "java -jar ../psppractica2/out/artifacts/Random10_jar/psppractica2.jar";
        }


        List<String> argsList = new ArrayList<>(Arrays.asList(comando.split(" ")));
        ProcessBuilder pb = new ProcessBuilder(argsList);
        String texto = "";
        String numero = "";
        File file = new File("randoms.txt");
        BufferedWriter bw2 = new BufferedWriter(new FileWriter(file));

        try{
            Process process = pb.start();
            OutputStream os = process.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            //Se guarda la salida del proceso secundario
            Scanner procesoSC = new Scanner(process.getInputStream());

            Scanner sc = new Scanner(System.in);

            String linea = sc.nextLine();
            //Se saca por pantalla la salida del proceso secundario cada vez que se escribe algo
            //Si se escribe stop parará
            while(!linea.equals("stop")){
                bw.write(linea);
                bw.newLine();
                bw.flush();
                texto = procesoSC.nextLine();
                System.out.println(texto);
                //Guardo en un String el texto de la salida del proceso secundario y lo guardo en el fichero randoms.txt
                bw2.write(texto+"\n");
                linea = sc.nextLine();
            }
            bw2.close();
        }catch (IOException ex){
            Logger.getLogger(Ejercicio2.class.getName());
        }
    }



}
