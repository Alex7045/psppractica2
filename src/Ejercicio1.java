import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;


public class Ejercicio1 {
    public static void main(String[] args) throws IOException,InterruptedException {
        //Si no  se pone ningun argumento falla
        if (args.length < 1) {
            System.err.println("Error, introduce un argumento");
            System.exit(-1);
        }
        ProcessBuilder pb = new ProcessBuilder(args);
        Process process = null;
        try {
            process = pb.start();
            //Si el programa tarda mas de 2 segundos interrumpe el programa y manda la excepción
            if (!process.waitFor(2,TimeUnit.SECONDS)){
                throw new InterruptedException();
            }
        } catch(IOException ex) {
            System.err.println("Error, comando no encontrado o mal escrito");
            System.exit(-1);
        } catch (InterruptedException ex){
            System.err.println("Tiempo agotado");
            System.exit(-1);
        }
        //Creo el archivo output donde se va a guardar la salida del comando
        File archivo = new File("output.txt");
        BufferedWriter bw = new BufferedWriter(new FileWriter(archivo));
        String texto = "";
        try{
            //Leo linea por linea el Stream que devuelve el comando y lo guardo en un String
            InputStream is = process.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            Scanner sc = new Scanner(br);
            while(sc.hasNext()){
                texto += sc.nextLine()+"\n";
            }
            //Finalmente lo guardo en el fichero
            bw.write(texto);
            bw.close();
            System.out.println("Proceso ejecutado correctamente, resultado almacenado en *output.txt*");
        }catch (Exception ex){
            System.err.println("Error de proceso hijo");
            System.exit(-1);
        }
    }
}
