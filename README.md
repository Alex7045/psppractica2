"# pspPractica2"  
Ejercicio1: Se ejecutara el jar correspondiente (java -jar), encontrado en out/artifacts/ejercicio1_jar/psppractica2 junto con una instrucción, por ejemplo: "java -jar out/artifacts/ejercicio1_jar/psppractica2 ls" (sin las comillas) el resultado de la ejecución se guardará en el fichero output.txt encontrado en la carpeta del proyecto.

Ejercicio2: Se ejecutará la clase main Ejercicio2.java y se escribirán a continuación cadenas de texto el programa devolverá números aleatorios (0-10) y los guardara en el fichero randoms.txt encontrado en la carpeta del proyecto.

Ejercicio3: Se ejecutará la clase main Ejercicio2.java y se escribirán a continuación cadenas de texto el programa devolverá las cadenas de texto en minúscula.

Link al código fuente: https://gitlab.com/Alex7045/psppractica2.git

